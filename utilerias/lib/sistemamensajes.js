$(function() 
{
    $("#divMessageBody").mouseover(function()
    {
        $("#divMessageBody").stop().animate({
            right: "0px"
        }, 700, "easeInOutCirc");
    });
});

function ShowMessage(msg) 
{
    clearInterval(msgBoxTimeout);
    $("#divMessageBody").css("right", msgBoxRight);
    $("#divMessage").append(msg);
    $("#divMessageBody").stop().animate({
        right: "0px"
    }, 700, "easeInOutCirc");

    msgBoxTimeout = setTimeout(function() {
        HideMessage();
    }, timeToShow);
}

function HideMessage()
{
    $("#divMessageBody").stop().animate({
        right: msgBoxRight
    }, 900, "easeInOutCirc");

    clearInterval(msgBoxTimeout);
}

$(function() 
{
    $("#divMessageBody").mouseout(function()
    {
            HideMessage();
    });
});